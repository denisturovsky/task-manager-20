package ru.tsc.denisturovsky.tm.command.user;

import ru.tsc.denisturovsky.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Log out";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() {
        getAuthService().logout();
    }

}
