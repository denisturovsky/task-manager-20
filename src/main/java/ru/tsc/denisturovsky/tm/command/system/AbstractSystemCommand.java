package ru.tsc.denisturovsky.tm.command.system;

import ru.tsc.denisturovsky.tm.api.service.ICommandService;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    public Role[] getRoles() {
        return null;
    }

}
