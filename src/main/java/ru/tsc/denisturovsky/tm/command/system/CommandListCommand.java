package ru.tsc.denisturovsky.tm.command.system;

import ru.tsc.denisturovsky.tm.api.model.ICommand;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Locale;

public final class CommandListCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-cmd";

    public static final String NAME = "commands";

    public static final String DESCRIPTION = "Show command list";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
