package ru.tsc.denisturovsky.tm.command.project;

import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    public static final String NAME = "project-list";

    public static final String DESCRIPTION = "Show project list";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort);
        System.out.printf("|%-30s:%30s:%30s:%30s:%30s:%30s|%n",
                "INDEX",
                "NAME",
                "STATUS",
                "DESCRIPTION",
                "DATE BEGIN",
                "DATE END"
        );
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.printf("|%-30s:%30s%n", index, project);
            index++;
        }
    }

}
