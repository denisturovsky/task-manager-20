package ru.tsc.denisturovsky.tm.command.task;

import ru.tsc.denisturovsky.tm.api.service.IProjectTaskService;
import ru.tsc.denisturovsky.tm.api.service.ITaskService;
import ru.tsc.denisturovsky.tm.command.AbstractCommand;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Task;
import ru.tsc.denisturovsky.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(final List<Task> tasks) {
        System.out.printf("|%-30s:%30s:%30s:%30s:%30s:%30s|%n",
                "INDEX",
                "NAME",
                "STATUS",
                "DESCRIPTION",
                "DATE BEGIN",
                "DATE END"
        );
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.printf("|%-30s:%30s%n", index, task);
            index++;
        }
    }

    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

}
