package ru.tsc.denisturovsky.tm.command.system;

import java.util.Locale;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Show program version";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.format("[%s] \n", NAME.toUpperCase(Locale.ROOT));
        System.out.format("1.20.0 \n");
    }

}
