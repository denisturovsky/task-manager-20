package ru.tsc.denisturovsky.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
