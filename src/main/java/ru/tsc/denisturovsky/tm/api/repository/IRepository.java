package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    boolean existsById(String id);

    M remove(M model);

    M removeOneById(String id);

    M removeOneByIndex(Integer index);

    M add(M model);

    void clear();

    int getSize();

}
