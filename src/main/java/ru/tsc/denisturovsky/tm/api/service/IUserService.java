package ru.tsc.denisturovsky.tm.api.service;

import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User userUpdate(String id, String firstName, String lastName, String middleName);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

}
