package ru.tsc.denisturovsky.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
