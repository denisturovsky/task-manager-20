package ru.tsc.denisturovsky.tm.enumerated;

import ru.tsc.denisturovsky.tm.exception.field.StatusEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.StatusIncorrectException;

public enum Status {

    NOT_STARTED("Not Started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed");

    private final String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    public static Status toStatus(final String value) {
        if (value == null || value.isEmpty()) throw new StatusEmptyException();
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        throw new StatusIncorrectException();
    }

    public String getDisplayName() {
        return displayName;
    }

}
